"use strict";

let gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel'),
    concat = require('gulp-concat'),
    less = require('gulp-less-sourcemap');

gulp.task('build-dist-js', () =>
    gulp.src(['js/*.js', 'js/*/*.js'])
        .pipe(sourcemaps.init())
        .pipe(babel({ presets: ['react', 'es2015']}))
        .pipe(concat('dist.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist'))
);

gulp.task('build-dist-less', () =>
    gulp.src(['less/*.less', 'less/*/*.less'])
        .pipe(concat('dist.css'))
        .pipe(less({
           sourceMap: {
               sourceMapURL: "dist.css.map",
               sourceMapRootpath: '../less',
               sourceMapFileInline: false
           }
        }))
        .pipe(gulp.dest('dist'))
);

gulp.task('watch', () => {
    gulp.watch(['js/*.js', 'js/*/*.js'], ['build-dist-js']);
    gulp.watch(['less/*.less', 'less/*/*.less'], ['build-dist-less']);
});

gulp.task('default', ['build-dist-js', 'build-dist-less']);