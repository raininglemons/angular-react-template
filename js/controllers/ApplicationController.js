"use strict";

function ApplicationController ($scope) {
    $scope.duck = 10;
}

angular.module("UntitledApp")
    .controller("ApplicationController", ApplicationController);
